package fr.univavignon.projet.ui.accueil;

import android.content.Intent;
import android.os.Bundle;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.AdapterView;
import android.widget.ArrayAdapter;
import android.widget.ImageView;
import android.widget.ListView;
import android.widget.TextView;
import android.widget.Toast;

import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import androidx.fragment.app.Fragment;
import androidx.lifecycle.Observer;
import androidx.lifecycle.ViewModelProviders;
import androidx.recyclerview.widget.DividerItemDecoration;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;

import java.util.Arrays;

import fr.univavignon.projet.R;
import fr.univavignon.projet.database_and_network.CeriItemList;
import fr.univavignon.projet.ui.itemSelected.ItemSelectedFragment;

public class AccueilFragment extends Fragment {

    private AccueilViewModel accueilViewModel;

    public View onCreateView(@NonNull LayoutInflater inflater,
                             ViewGroup container, Bundle savedInstanceState)
    {
        accueilViewModel =
                ViewModelProviders.of(this).get(AccueilViewModel.class);
        final View root = inflater.inflate(R.layout.fragment_accueil, container, false);
        /*final TextView textView = root.findViewById(R.id.text_home);
        accueilViewModel.getText().observe(getViewLifecycleOwner(), new Observer<String>() {
            @Override
            public void onChanged(@Nullable String s) {
                textView.setText(s);
            }
        });*/

        /*final RecyclerView rv = (RecyclerView) root.findViewById(R.id.recyclerView);
        rv.setLayoutManager(new LinearLayoutManager(getActivity()));
        //rv.setAdapter();*/

        //placeholder data because I can't receive data from the server
        final ListView listview = root.findViewById(R.id.lstView);
        String[] arrayToSort = CeriItemList.getNameArray();
        Arrays.sort(arrayToSort);
        final ArrayAdapter<String> adapter = new ArrayAdapter<String>(root.getContext(), android.R.layout.simple_list_item_1, arrayToSort);
        //final ArrayAdapter<String> adapter = new ArrayAdapter<String>(root.getContext(), android.R.layout.simple_list_item_1, CeriItemList.getNameArray());
        listview.setAdapter(adapter);

        listview.setOnItemClickListener(new AdapterView.OnItemClickListener()
        {
            public void onItemClick(AdapterView parent, View v, int position, long id)
            {
                // Do something in response to the click
                final String item = (String) parent.getItemAtPosition(position);
                Toast.makeText(root.getContext(), "Objet sélectionnée: " + item, Toast.LENGTH_LONG).show();
                //Intent objetInfo = new Intent(parent.getContext(), ItemSelectedFragment.class);
                //objetInfo.putExtra("name", (String) parent.getItemAtPosition(position));
                //startActivity(objetInfo);
            }
        });
        return root;
    }




}
