package fr.univavignon.projet.ui.categories;

import android.content.Intent;
import android.os.Bundle;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.AdapterView;
import android.widget.ArrayAdapter;
import android.widget.ListView;
import android.widget.TextView;
import android.widget.Toast;

import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import androidx.fragment.app.Fragment;
import androidx.lifecycle.Observer;
import androidx.lifecycle.ViewModelProviders;

import java.util.Arrays;

import fr.univavignon.projet.R;
import fr.univavignon.projet.ui.categoriesSelected.CategorieSelectedFragment;

public class CategoriesFragment extends Fragment {

    private CategoriesViewModel categoriesViewModel;

    public View onCreateView(@NonNull LayoutInflater inflater,
                             ViewGroup container, Bundle savedInstanceState)
    {
        categoriesViewModel =
                ViewModelProviders.of(this).get(CategoriesViewModel.class);
        final View root = inflater.inflate(R.layout.fragment_categories, container, false);
        /*final TextView textView = root.findViewById(R.id.text_categories);
        categoriesViewModel.getText().observe(getViewLifecycleOwner(), new Observer<String>() {
            @Override
            public void onChanged(@Nullable String s) {
                textView.setText(s);
            }
        });*/

        final ListView listView = root.findViewById(R.id.lstView);

        //placeholder data because I can't receive data from the server
        String[] categoriesArray = new String[]{"écran","station de travail","periphérique","8 bits","SCSI","ordinateur","câble/adaptateur","règle à calcul","réseau","support de stockage","ordinateur de bureau","ordinateur de poche","ordinateur portable","terminal de communication","composant","téléphone","périphérique","prototype"};
        Arrays.sort(categoriesArray);

        final ArrayAdapter<String> adapter = new ArrayAdapter<String>(root.getContext(), android.R.layout.simple_list_item_1, categoriesArray);
        listView.setAdapter(adapter);

        listView.setOnItemClickListener(new AdapterView.OnItemClickListener()
        {
            public void onItemClick(AdapterView parent, View v, int position, long id)
            {
                // Do something in response to the click
                final String item = (String) parent.getItemAtPosition(position);
                Toast.makeText(root.getContext(), "Catégorie sélectionnée: " + item, Toast.LENGTH_LONG).show();
                //Intent categorieInfo = new Intent(parent.getContext(), CategorieSelectedFragment.class);
                //categorieInfo.putExtra("name", (String) parent.getItemAtPosition(position));
                //startActivity(categorieInfo);
            }
        });
        return root;
    }
}
