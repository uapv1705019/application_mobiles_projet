package fr.univavignon.projet.ui.itemSelected;

import androidx.lifecycle.LiveData;
import androidx.lifecycle.MutableLiveData;
import androidx.lifecycle.ViewModel;

public class ItemSelectedViewModel extends ViewModel {

    private MutableLiveData<String> mText;

    public ItemSelectedViewModel() {
        mText = new MutableLiveData<>();
        mText.setValue("Page une fois un item sélectionné");
    }

    public LiveData<String> getText() {
        return mText;
    }
}