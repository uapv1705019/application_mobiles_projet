package fr.univavignon.projet.ui.categoriesSelected;

import androidx.lifecycle.LiveData;
import androidx.lifecycle.MutableLiveData;
import androidx.lifecycle.ViewModel;

public class CategorieSelectedViewModel extends ViewModel {

    private MutableLiveData<String> mText;

    public CategorieSelectedViewModel() {
        mText = new MutableLiveData<>();
        mText.setValue("Page des catégories une fois sélectionnée");
    }

    public LiveData<String> getText() {
        return mText;
    }
}