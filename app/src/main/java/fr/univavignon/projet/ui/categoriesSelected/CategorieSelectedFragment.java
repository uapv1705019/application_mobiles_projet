package fr.univavignon.projet.ui.categoriesSelected;

import android.os.Bundle;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;

import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import androidx.fragment.app.Fragment;
import androidx.lifecycle.Observer;
import androidx.lifecycle.ViewModelProviders;

import fr.univavignon.projet.R;

public class CategorieSelectedFragment extends Fragment {

    private CategorieSelectedViewModel categorieSelectedViewModel;

    public View onCreateView(@NonNull LayoutInflater inflater,
                             ViewGroup container, Bundle savedInstanceState) {
        categorieSelectedViewModel =
                ViewModelProviders.of(this).get(CategorieSelectedViewModel.class);
        View root = inflater.inflate(R.layout.fragment_categorie_selected, container, false);
        final TextView textView = root.findViewById(R.id.text_categories_selected);
        categorieSelectedViewModel.getText().observe(getViewLifecycleOwner(), new Observer<String>() {
            @Override
            public void onChanged(@Nullable String s) {
                textView.setText(s);
            }
        });
        return root;
    }
}
