package fr.univavignon.projet.ui.rechercher;

import androidx.lifecycle.LiveData;
import androidx.lifecycle.MutableLiveData;
import androidx.lifecycle.ViewModel;

public class RechercherViewModel extends ViewModel {

    private MutableLiveData<String> mText;

    public RechercherViewModel() {
        mText = new MutableLiveData<>();
        mText.setValue("Page de recherche");
    }

    public LiveData<String> getText() {
        return mText;
    }
}