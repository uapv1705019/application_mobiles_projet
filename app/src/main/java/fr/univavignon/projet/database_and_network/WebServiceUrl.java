package fr.univavignon.projet.database_and_network;

import android.net.Uri;

import java.net.MalformedURLException;
import java.net.URL;

public class WebServiceUrl
{

    private static final String HOST = "demo-lia.univ-avignon.fr";
    private static final String PATH_1 = "cerimuseum";

    private static Uri.Builder commonBuilder()
    {
        Uri.Builder builder = new Uri.Builder();

        builder.scheme("https")
                .authority(HOST)
                .appendPath(PATH_1);
        return builder;
    }

    // Get all IDs of the museum items
    // https://https://demo-lia.univ-avignon.fr/cerimuseum/ids
    private static final String SEARCH_IDS = "ids";

    // Build URL to get all IDs of the museum items
    public static URL buildSearchIds() throws MalformedURLException
    {
        Uri.Builder builder = commonBuilder();
        builder.appendPath(SEARCH_IDS);
        URL url = new URL(builder.build().toString());
        return url;
    }

    // Get all categories of the museum items
    // https://https://demo-lia.univ-avignon.fr/cerimuseum/categories
    private static final String SEARCH_CATEGORIES = "categories";

    // Build URL to get all categories of the museum items
    public static URL buildSearchCategories() throws MalformedURLException
    {
        Uri.Builder builder = commonBuilder();
        builder.appendPath(SEARCH_CATEGORIES);
        URL url = new URL(builder.build().toString());
        return url;
    }

    // Get the complete catalog of the museum items
    // https://https://demo-lia.univ-avignon.fr/cerimuseum/catalog
    private static final String SEARCH_CATALOG = "catalog";

    // Build URL to get the complete catalog of the museum items
    public static URL buildSearchCatalog() throws MalformedURLException
    {
        Uri.Builder builder = commonBuilder();
        builder.appendPath(SEARCH_CATALOG);
        URL url = new URL(builder.build().toString());
        return url;
    }

    // Get the caracterictics of the museum item given in parameters
    // https://https://demo-lia.univ-avignon.fr/cerimuseum/items/<itemID>
    private static final String SEARCH_ITEM = "items";

    // Build URL to get the caracterictics of the museum item given in parameters
    public static URL buildSearchItem(String itemID) throws MalformedURLException
    {
        Uri.Builder builder = commonBuilder();
        builder.appendPath(SEARCH_ITEM)
                .appendPath(String.valueOf(itemID));
        URL url = new URL(builder.build().toString());
        return url;
    }

    // Get the thumbnail of the museum item given in parameters
    // https://https://demo-lia.univ-avignon.fr/cerimuseum/items/<itemID>/thumbnail
    private static final String SEARCH_THUMBNAIL = "thumbnail";

    // Build URL to get the thumbnail of the museum item given in parameters
    public static URL buildSearchItemThumbnail(String itemID) throws MalformedURLException
    {
        Uri.Builder builder = commonBuilder();
        builder.appendPath(SEARCH_ITEM)
                .appendPath(String.valueOf(itemID))
                .appendPath(SEARCH_THUMBNAIL);
        URL url = new URL(builder.build().toString());
        return url;
    }

    // Get the picture of the museum item given in parameters and with his image ID
    // https://https://demo-lia.univ-avignon.fr/cerimuseum/items/<itemID>/images/<imageID>
    private static final String SEARCH_IMAGES = "images";

    // Build URL to get the picture of the museum item given in parameters and with his image ID
    public static URL buildSearchItemImage(String itemID, String imageID) throws MalformedURLException
    {
        Uri.Builder builder = commonBuilder();
        builder.appendPath(SEARCH_ITEM)
                .appendPath(String.valueOf(itemID))
                .appendPath(SEARCH_IMAGES)
                .appendPath(String.valueOf(imageID));
        URL url = new URL(builder.build().toString());
        return url;
    }

    // Get all demos date of the museum
    // https://https://demo-lia.univ-avignon.fr/cerimuseum/demos
    private static final String SEARCH_DEMOS = "demos";

    // Build URL to get all IDs of the museum items
    public static URL buildSearchDemos() throws MalformedURLException
    {
        Uri.Builder builder = commonBuilder();
        builder.appendPath(SEARCH_DEMOS);
        URL url = new URL(builder.build().toString());
        return url;
    }


}
