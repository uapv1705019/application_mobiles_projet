package fr.univavignon.projet.database_and_network;

import java.util.HashMap;

public class CeriItemList {

    private static HashMap<String, CeriItem> hashMap = init();

    private static HashMap<String, CeriItem> init() {
        HashMap<String, CeriItem> res = new HashMap<>();
        res.put("Macintosh PowerBook 145", new CeriItem("tsx", "Macintosh PowerBook 145"));
        res.put("Câbles et terminateurs SCSI", new CeriItem("yzw", "Câbles et terminateurs SCSI"));
        res.put("Système de sauvegarde sur bande", new CeriItem("ace", "Système de sauvegarde sur bande"));
        res.put("HP 9000 Series 712-60", new CeriItem("q2u", "HP 9000 Series 712-60"));
        res.put("Macintosh Plus", new CeriItem("jjh", "Macintosh Plus"));
        res.put("PDA iPAQ 3630", new CeriItem("w1s", "PDA iPAQ 3630"));
        res.put("Étiquettes de protection pour disquettes 5¼ pouces", new CeriItem("cza", "Étiquettes de protection pour disquettes 5¼ pouces"));
        res.put("ZX81", new CeriItem("g01", "ZX81"));
        res.put("Téléviseur N&B", new CeriItem("flh", "Téléviseur N&B"));
        res.put("Alice", new CeriItem("2j7", "Alice"));
        res.put("TO7/70", new CeriItem("tnx", "TO7/70"));
        res.put("Disque dur", new CeriItem("ave", "Disque dur"));
        res.put("Modem F@st 1000", new CeriItem("rxs", "Modem F@st 1000"));
        res.put("Règle à calcul", new CeriItem("51a", "Règle à calcul"));
        res.put("Cartes PCMCIA", new CeriItem("0qf", "Cartes PCMCIA"));
        res.put("ScanMan 32", new CeriItem("4w6", "ScanMan 32"));
        res.put("C64", new CeriItem("r6j", "C64"));
        res.put("Calculatrice du programmeur", new CeriItem("cdn", "Calculatrice du programmeur"));
        res.put("Téléphone GSM", new CeriItem("4ar", "Téléphone GSM"));
        res.put("Lecteur de disquettes pour TO7", new CeriItem("ci6", "Lecteur de disquettes pour TO7"));
        res.put("Toughbook", new CeriItem("kyt", "Toughbook"));
        res.put("Bouchon pour Ethernet 10Base2", new CeriItem("lf8", "Bouchon pour Ethernet 10Base2"));
        res.put("Jauge de contrôle pour cartes perforées", new CeriItem("xnw", "Jauge de contrôle pour cartes perforées"));
        res.put("Cartouche Mémo7", new CeriItem("blm", "Cartouche Mémo7"));
        res.put("Bandes magnétiques", new CeriItem("9lr", "Bandes magnétiques"));
        res.put("Souris pour station HP", new CeriItem("rkj", "Souris pour station HP"));
        res.put("Disquettes 3½ pouces double densité", new CeriItem("gex", "Disquettes 3½ pouces double densité"));
        res.put("MAU (transceivers) Ethernet", new CeriItem("80t", "MAU (transceivers) Ethernet"));
        res.put("Cartouche de jeu NES", new CeriItem("mfj", "Cartouche de jeu NES"));
        res.put("Apple //c", new CeriItem("k0j", "Apple //c"));
        res.put("Monitor //c", new CeriItem("jne", "Monitor //c"));
        res.put("PC portable", new CeriItem("9x3", "PC portable"));
        res.put("Joysticks pour TO7", new CeriItem("hjy", "Joysticks pour TO7"));
        res.put("Barrettes de RAM", new CeriItem("jxv", "Barrettes de RAM"));
        res.put("Minitel 1", new CeriItem("lon", "Minitel 1"));
        res.put("Minitel 2", new CeriItem("ry8", "Minitel 2"));
        res.put("Lecteur de cartouches amovibles 88 Mio", new CeriItem("hsv", "Lecteur de cartouches amovibles 88 Mio"));
        res.put("Lecteur de cassettes pour TO7", new CeriItem("8xi", "Lecteur de cassettes pour TO7"));
        res.put("Mémoire à tores de ferrite", new CeriItem("c1z", "Mémoire à tores de ferrite"));
        res.put("Cartouche à disque dur Jaz", new CeriItem("g1c", "Cartouche à disque dur Jaz"));
        res.put("MTM", new CeriItem("4zh", "MTM"));
        return res;
    }

    public static String[] getNameArray()
    {
        return hashMap.keySet().toArray(new String[hashMap.size()]);
    }

    public static CeriItem getCeriItem(String name)
    {
        return hashMap.get(name);
    }


}


