package fr.univavignon.projet.database_and_network;

import android.content.ContentValues;
import android.content.Context;
import android.database.Cursor;
import android.database.sqlite.SQLiteDatabase;
import android.database.sqlite.SQLiteOpenHelper;
import android.util.Log;

import java.util.ArrayList;
import java.util.List;

import static android.database.sqlite.SQLiteDatabase.CONFLICT_IGNORE;

public class CeriItemDbHelper extends SQLiteOpenHelper
{

    private static final String TAG = CeriItemDbHelper.class.getSimpleName();

    // If you change the database schema, you must increment the database version.
    private static final int DATABASE_VERSION = 1;

    public static final String DATABASE_NAME = "ceriItem.db";

    public static final String TABLE_NAME = "ceriItem";

    public static final String _ID = "_id";
    public static final String COLUMN_ITEM_ID = "itemId";
    public static final String COLUMN_ITEM_NAME = "name";
    public static final String COLUMN_ITEM_CATEGORIES = "categories";
    public static final String COLUMN_ITEM_DESCRIPTION = "description";
    public static final String COLUMN_ITEM_TIME_FRAME = "timeFrame";
    public static final String COLUMN_ITEM_YEAR = "year";
    public static final String COLUMN_ITEM_BRAND = "brand";
    public static final String COLUMN_ITEM_TECHNICAL_DETAILS = "technicalDetails";
    public static final String COLUMN_ITEM_WORKING = "working";
    public static final String COLUMN_ITEM_PICTURES = "pictures";

    private SQLiteDatabase database; //adapté du code du TP2 de Virgile Sucal
    public static CeriItemDbHelper CERI_ITEM_DB_HELPER; //adapté du code du TP2 de Virgile Sucal

    public CeriItemDbHelper(Context context)
    {
        super(context, DATABASE_NAME, null, DATABASE_VERSION);
        CERI_ITEM_DB_HELPER = this; //adapté du code du TP2 de Virgile Sucal
    }



    @Override
    public void onCreate(SQLiteDatabase db)
    {
        final String SQL_CREATE_BOOK_TABLE = "CREATE TABLE " + TABLE_NAME + " (" +
                _ID + " INTEGER PRIMARY KEY," +
                COLUMN_ITEM_ID + "TEXT NOT NULL," +
                COLUMN_ITEM_NAME + " TEXT NOT NULL, " +
                COLUMN_ITEM_CATEGORIES + "TEXT," +
                COLUMN_ITEM_DESCRIPTION + "TEXT," +
                COLUMN_ITEM_TIME_FRAME + "INTEGER," +
                COLUMN_ITEM_YEAR + "INTEGER," +
                COLUMN_ITEM_BRAND + "TEXT," +
                COLUMN_ITEM_TECHNICAL_DETAILS + "TEXT," +
                COLUMN_ITEM_WORKING + "BOOLEAN," +
                COLUMN_ITEM_PICTURES + "TEXT" +
                ");";

        db.execSQL(SQL_CREATE_BOOK_TABLE);
        setDataBase(db); //adapté du code du TP2 de Virgile Sucal
        this.populate();
    }

    @Override
    public void onUpgrade(SQLiteDatabase db, int oldVersion, int newVersion)
    {
        db.execSQL("DROP TABLE IF EXISTS " + TABLE_NAME);
        onCreate(db);
    }

    /**
     * Fills ContentValues result from a CeriItem object
     */
    private ContentValues fill(CeriItem item)
    {

        ContentValues values = new ContentValues();

        //TODO complete this

        return values;
    }

    /**
     * Updates the information of an CERI item inside the data base
     * @return the number of updated rows
     */
    public int updateitem(CeriItem item)
    {
        //SQLiteDatabase db = this.getWritableDatabase();
        SQLiteDatabase db = getDatabase();

        ContentValues values = fill(item);

        // updating row
        return db.updateWithOnConflict(TABLE_NAME, values, _ID + " = ?",
                new String[] { String.valueOf(item.getId()) }, CONFLICT_IGNORE);
    }


    /**
     * Returns a cursor on all the items of the data base
     */
    public Cursor fetchAllItems()
    {
        //SQLiteDatabase db = this.getReadableDatabase();
        SQLiteDatabase db = getDatabase();

        Cursor cursor = db.query(TABLE_NAME, null,
                null, null, null, null, COLUMN_ITEM_NAME +" ASC", null);

        Log.d(TAG, "call fetchAllItems()");
        if (cursor != null)
        {
            cursor.moveToFirst();
        }
        return cursor;
    }

    /**
     * Returns a list on all the items of the data base
     */
    public List<CeriItem> getAllItems()
    {
        List<CeriItem> res = new ArrayList<>();
        Cursor cursor = fetchAllItems();
        while(cursor.moveToNext())
        {
            res.add(cursorToItem(cursor));
        }
        return res;
    }

    private void populate()
    {
        // TODO complete this
    }

    public CeriItem cursorToItem(Cursor cursor)
    {
        CeriItem item = new CeriItem(cursor.getLong(cursor.getColumnIndex(_ID)),
                cursor.getString(cursor.getColumnIndex(COLUMN_ITEM_ID)),
                cursor.getString(cursor.getColumnIndex(COLUMN_ITEM_NAME))
                //TODO add categories
        );
        return item;
    }

    public CeriItem getItem(int id)
    {
        CeriItem item = null;
        CeriItem searchItem = null;
        Cursor cursor = fetchAllItems();
        while(cursor.moveToNext())
        {
            searchItem = cursorToItem(cursor);
            if(searchItem.getId() == id)
            {
                item = searchItem;
                break;
            }
        }
        return item;
    }

    public SQLiteDatabase getDatabase() //adapté du code du TP2 de Virgile Sucal
    {
        if (this.database == null)
        {
            this.database = this.getWritableDatabase();
        }

        return this.database;
    }

    public SQLiteDatabase setDataBase(SQLiteDatabase db) //adapté du code du TP2 de Virgile Sucal
    {
        if (this.database != null)
        {
            this.database.close();
        }
        this.database = db;

        return this.database;
    }


}
