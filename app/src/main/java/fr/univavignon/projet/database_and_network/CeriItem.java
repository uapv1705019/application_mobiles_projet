package fr.univavignon.projet.database_and_network;

import android.os.Build;
import android.os.Parcel;
import android.os.Parcelable;

import androidx.annotation.RequiresApi;

import java.util.Dictionary;

public class CeriItem implements Parcelable
{

    public static final String TAG = CeriItem.class.getSimpleName();

    private long id; // used for the _id colum of the db helper

    private String idItem;
    private String name;
    private String[] categories;
    private String description;
    private int[] timeFrame;
    private int year;
    private String brand;
    private String[] technicalDetails;
    private Boolean working;
    private Dictionary pictures;


    public CeriItem(long id, String idItem, String name, String[] categories, String description, int[] timeFrame, int year, String brand, String[] technicalDetails, Boolean working, Dictionary pictures)
    {
        this.id = id;
        this.idItem = idItem;
        this.name = name;
        this.categories = categories;
        this.description = description;
        this.timeFrame = timeFrame;
        this.year = year;
        this.brand = brand;
        this.technicalDetails = technicalDetails;
        this.working = working;
        this.pictures = pictures;
    }

    public CeriItem(String idItem, String name, String[] categories, String description, int[] timeFrame, int year, String brand, String[] technicalDetails, Boolean working, Dictionary pictures)
    {
        this.idItem = idItem;
        this.name = name;
        this.categories = categories;
        this.description = description;
        this.timeFrame = timeFrame;
        this.year = year;
        this.brand = brand;
        this.technicalDetails = technicalDetails;
        this.working = working;
        this.pictures = pictures;
    }

    public CeriItem(long id, String idItem, String name, String[] categories)
    {
        this.id = id;
        this.idItem = idItem;
        this.name = name;
        this.categories = categories;
    }

    public CeriItem(long id, String idItem, String name)
    {
        this.id = id;
        this.idItem = idItem;
        this.name = name;
    }

    public CeriItem(String idItem, String name)
    {
        this.idItem = idItem;
        this.name = name;
    }

    public CeriItem(String idItem, String name, String[] categories)
    {
        this.idItem = idItem;
        this.name = name;
        this.categories = categories;
    }

    public CeriItem()
    {
    }

    public long getId() {
        return id;
    }

    public void setId(long id) {
        this.id = id;
    }

    public String getIdItem() {
        return idItem;
    }

    public void setIdItem(String idItem) {
        this.idItem = idItem;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String[] getCategories() {
        return categories;
    }

    public void setCategories(String[] categories) {
        this.categories = categories;
    }

    public String getDescription() {
        return description;
    }

    public void setDescription(String description) {
        this.description = description;
    }

    public int[] getTimeFrame() {
        return timeFrame;
    }

    public void setTimeFrame(int[] timeFrame) {
        this.timeFrame = timeFrame;
    }

    public int getYear() {
        return year;
    }

    public void setYear(int year) {
        this.year = year;
    }

    public String getBrand() {
        return brand;
    }

    public void setBrand(String brand) {
        this.brand = brand;
    }

    public String[] getTechnicalDetails() {
        return technicalDetails;
    }

    public void setTechnicalDetails(String[] technicalDetails) {
        this.technicalDetails = technicalDetails;
    }

    public Boolean getWorking() {
        return working;
    }

    public void setWorking(Boolean working) {
        this.working = working;
    }

    public Dictionary getPictures() {
        return pictures;
    }

    public void setPictures(Dictionary pictures) {
        this.pictures = pictures;
    }

    @Override
    public String toString()
    {
        return this.name+"("+this.brand +")";
    }

    @Override
    public int describeContents() {
        return 0;
    }

    @RequiresApi(api = Build.VERSION_CODES.Q)
    @Override
    public void writeToParcel(Parcel dest, int flags)
    {
    dest.writeLong(id);
    dest.writeString(name);
    dest.writeStringArray(categories);
    dest.writeString(description);
    dest.writeIntArray(timeFrame);
    dest.writeInt(year);
    dest.writeString(brand);
    dest.writeStringArray(technicalDetails);
    dest.writeBoolean(working);
    }

    public static final Parcelable.Creator<CeriItem> CREATOR = new Parcelable.Creator<CeriItem>()
    {
        @Override
        public CeriItem createFromParcel(Parcel source)
        {
            return new CeriItem(source);
        }

        @Override
        public CeriItem[] newArray(int size)
        {
            return new CeriItem[size];
        }
    };

    public CeriItem(Parcel in)
    {
        //TODO do something
    }


}
